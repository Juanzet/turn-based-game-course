using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    const float MIN_FOLLOW_Y_OFFSET = 2f;
    const float MAX_FOLLOW_Y_OFFSET = 12f;
    [SerializeField] CinemachineVirtualCamera cinemachineVirtualCamera;
    CinemachineTransposer cinemachineTransposer;
    Vector3 targetFollowOffset;
    float moveSpeed = 5f;
    float rotationSpeed = 100f;
    float zoomAmount = 1f;
    float zoomSpeed = 5f;

    void Start() 
    {
        cinemachineTransposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();    
        targetFollowOffset = cinemachineTransposer.m_FollowOffset;
    }
    
    void Update()
    {
       HandleMovement();
       HandleRotation();
       HandleZoom();
    }

    void HandleMovement()
    {
         Vector3 inputMoveDir = new Vector3(0,0,0);
        if(Input.GetKey(KeyCode.W))
        {
            inputMoveDir.z = +1f;
        }   
        if(Input.GetKey(KeyCode.S))
        {
            inputMoveDir.z = -1f;
        }  
        if(Input.GetKey(KeyCode.A))
        {
            inputMoveDir.x = -1f;
        }  
        if(Input.GetKey(KeyCode.D))
        {
            inputMoveDir.x = +1f;
        }

        Vector3 moveVector = transform.forward * inputMoveDir.z + transform.right * inputMoveDir.x;
        transform.position += moveVector * moveSpeed * Time.deltaTime;   

    }

    void HandleRotation() 
    {
         Vector3 rotationvector = new Vector3(0,0,0);

        if(Input.GetKey(KeyCode.Q))
        {
            rotationvector.y = +1f;
        }
         if(Input.GetKey(KeyCode.E))
        {
            rotationvector.y = -1f;
        }

        transform.eulerAngles += rotationvector * rotationSpeed * Time.deltaTime;
    }

    void HandleZoom()
    {
         Vector3 followOffset = cinemachineTransposer.m_FollowOffset;
        if(Input.mouseScrollDelta.y > 0)
        {
           //var smooth = cinemachineTransposer.m_YDamping = 5f;
            targetFollowOffset.y -= zoomAmount;
        }
        if(Input.mouseScrollDelta.y < 0)
        {
            //var smooth = cinemachineTransposer.m_YDamping = 5f;
            targetFollowOffset.y += zoomAmount;
        }
        targetFollowOffset.y = Mathf.Clamp(targetFollowOffset.y, MIN_FOLLOW_Y_OFFSET, MAX_FOLLOW_Y_OFFSET);
        cinemachineTransposer.m_FollowOffset = Vector3.Lerp(cinemachineTransposer.m_FollowOffset, targetFollowOffset, Time.deltaTime * zoomSpeed);
    }

}
