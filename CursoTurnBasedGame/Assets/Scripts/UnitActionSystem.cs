using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitActionSystem : MonoBehaviour
{
    public static UnitActionSystem Instance { get; private set; }
    public event EventHandler OnSelectedUnitChanged;
   [SerializeField] Unit selectedUnit;
   [SerializeField] LayerMask unitLayerMask;

    void Awake() 
    {
        if(Instance != null)
        {
            Debug.LogError("Hay mas de un UnitActionSystem! " + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
       Instance = this;    
    }
   void Update() 
   {
       if(Input.GetMouseButton(0))
        {
            if (TryHandleUnitSelection()) return;

            selectedUnit.Move(MouseWorld.GetPosition());
        } 
   }
    
    bool TryHandleUnitSelection()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);   
        if(Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue,unitLayerMask))
        {
            if(raycastHit.transform.TryGetComponent<Unit>(out Unit unit))
            {
                SetSelectedUnit(unit);
                return true;
            }
        }    

        return false;    
    }

    void SetSelectedUnit(Unit unit)
    {
        selectedUnit = unit;
        OnSelectedUnitChanged?.Invoke(this,EventArgs.Empty);

        /* Es lo mismo que usar  OnSelectedUnitChanged?.Invoke(this,EventArgs.Empty);
        if(OnSelectedUnitChanged != null)
        {
            OnSelectedUnitChanged(this,EventArgs.Empty);
        }
        */
    }

    public Unit GetSelectedUnit()
    {
        return selectedUnit;        
    }
}
