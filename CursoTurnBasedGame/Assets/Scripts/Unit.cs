using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField] Animator unitAnimator;
    Vector3 targetPosition;
    GridPosition gridPosition;
    float moveSpeed = 4f;
    float rotateSpeed = 10f;
    float stoppingDistance = .1f;

    void Awake() 
    {
        targetPosition = transform.position;
    }
    void Start() 
    {
        gridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        LevelGrid.Instance.AddUnitAtGridPosition(gridPosition, this);
    }
    void Update()
    {
        if(Vector3.Distance(transform.position,targetPosition) > stoppingDistance)
        {
             Vector3 moveDirection = (targetPosition - transform.position).normalized;
             transform.position += moveDirection * moveSpeed * Time.deltaTime;
             //transform.forward = moveDirection; // metodo sencillo
             transform.forward = Vector3.Lerp(transform.forward,moveDirection, Time.deltaTime * rotateSpeed);
             unitAnimator.SetBool("IsWalking", true);
        } 
        else 
        {
            unitAnimator.SetBool("IsWalking", false);
        }

        GridPosition newGridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        if(newGridPosition != gridPosition)
        {
            //unit changed grid position
            LevelGrid.Instance.UnitMovedGridPosition(this, gridPosition, newGridPosition);
            gridPosition = newGridPosition;
        }
    }

    public void Move(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
    }
    
}
