using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GridDebugObject : MonoBehaviour
{
    [SerializeField] TextMeshPro TextMeshPro;
   GridObject gridObject;
   public void SetGridObject(GridObject gridObject)
   {
        this.gridObject = gridObject;
   }

   void Update() 
   {
        TextMeshPro.text = gridObject.ToString();
   }
}
