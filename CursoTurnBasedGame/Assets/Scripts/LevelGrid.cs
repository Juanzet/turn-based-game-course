using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour
{
    public static LevelGrid Instance { get; private set; }
  [SerializeField] Transform gridDebugObjectPrefab;
    GridSystem gridSystem;

    void Awake()
    {
         if(Instance != null)
        {
            Debug.LogError("Hay mas de un UnitActionSystem! " + transform + " - " + Instance);
            Destroy(gameObject);
            return;
        }
       Instance = this;    

        gridSystem = new GridSystem(10,10, 2f);
        gridSystem.CreateDebugObjects(gridDebugObjectPrefab);
    }

    public void AddUnitAtGridPosition(GridPosition gridPosition, Unit unit) 
    {
        GridObject gridObject = gridSystem.GetGridObject(gridPosition);
        gridObject.AddUnit(unit);
    }

    public List<Unit> GetUnitAtGridPosition(GridPosition gridPosition, Unit unit) 
    {
        GridObject gridObject = gridSystem.GetGridObject(gridPosition);
        return gridObject.GetUnitList();
    }

    public void RemoveUnitAtGridPosition(GridPosition gridPosition, Unit unit) 
    {
        GridObject gridObject = gridSystem.GetGridObject(gridPosition);
        gridObject.RemoveUnit(unit);
    }

    public void UnitMovedGridPosition(Unit unit, GridPosition fromGridPosition, GridPosition toGridPosition)
    {
        RemoveUnitAtGridPosition(fromGridPosition, unit);

        AddUnitAtGridPosition(toGridPosition, unit);
    }

    public GridPosition GetGridPosition(Vector3 worldPosition) => gridSystem.GetGridPosition(worldPosition);

#region  investigar porque esta forma no lo permite pero la de arriba si, entiendo que de una forma es explicita y la otra implicita pero investigar mas
/*
     public GridPosition GetGridPosition(Vector3 worldPosition)
     {
        return gridSystem.GetGridPosition(worldPosition);
     }
     */
#endregion
 
}
